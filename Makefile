
all: build/Makefile
	$(MAKE) -C build maps


build/Makefile: gen-makefile
	./gen-makefile

install: 
	mkdir -p $(DESTDIR)/usr/share/keymaps/xkb
	install -m644 build/*.map.gz $(DESTDIR)/usr/share/keymaps/xkb

clean:
	rm -r build
